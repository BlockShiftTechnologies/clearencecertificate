import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders ,HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs/Rx';




@Injectable()
export class DataService {

  constructor(private http:HttpClient) { }



 registeradmin(client_username,client_password,client_email,client_designation):Observable<any>{
 
console.log('Username received from client',client_username);
console.log('Password received from client',client_password);
console.log('Designation received from client',client_email);
console.log('Mobile received from client',client_designation);
 
const body = new HttpParams()
    .set('username', client_username)
    .set('password', client_password)
    .set('email',client_email)
    .set('designation',client_designation);

 return this.http.post('http://ec2-52-91-159-152.compute-1.amazonaws.com:3000/registeradmin',
    body,
    {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
    }
  );




 }



loginadmin(client_email,client_password):Observable<any>{
  
  console.log("Admin Username",client_email);
  console.log("Admin password",client_password);

const body = new HttpParams()
    .set('email',client_email)
    .set('password',client_password);


return this.http.post('http://ec2-52-91-159-152.compute-1.amazonaws.com:3000/login',body,

{
  headers: new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded')
}

);

}



searchstudent(enrollment_no):Observable<any>{
  
console.log("Enrollment Number",enrollment_no);

const body = new HttpParams()
        .set('enrollmentno',enrollment_no);

const headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');

return this.http.post('http://ec2-52-91-159-152.compute-1.amazonaws.com:3000/searchstudent',body,{headers});


}


fetchallstudent(){
  
return this.http.get('http://ec2-52-91-159-152.compute-1.amazonaws.com:3000/fetchallstudent');



}


updatestatus(enrollmentno,status){
  
  console.log("Server information",enrollmentno);
    console.log("Server information",status);
  const body = new HttpParams()
        .set('enrollmentno',enrollmentno)
        .set('student_status',status);
const headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');

return this.http.post('http://ec2-52-91-159-152.compute-1.amazonaws.com:3000/updatestudentstatus',body,{headers});

}



}
