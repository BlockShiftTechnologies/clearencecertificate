import { Component, OnInit } from '@angular/core';
import {DataService} from '../data.service';
import {MatTableDataSource} from '@angular/material';
@Component({
  selector: 'app-listclearedstudent',
  templateUrl: './listclearedstudent.component.html',
  styleUrls: ['./listclearedstudent.component.css']
})
export class ListclearedstudentComponent implements OnInit {

  constructor(private dataservice:DataService) { }

displayedColumns = ['StudentName', 'StudentBatch', 'StudentEnrollment','StudentStatus'];
dataSource = new MatTableDataSource();
ngOnInit() {

var dataretreived ; 
dataretreived = this.dataservice.fetchallstudent();
console.log("Fetch data ",dataretreived);
this.dataSource = dataretreived;
  }

}
