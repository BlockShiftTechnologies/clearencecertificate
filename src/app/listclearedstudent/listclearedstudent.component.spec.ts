import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListclearedstudentComponent } from './listclearedstudent.component';

describe('ListclearedstudentComponent', () => {
  let component: ListclearedstudentComponent;
  let fixture: ComponentFixture<ListclearedstudentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListclearedstudentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListclearedstudentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
