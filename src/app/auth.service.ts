import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie';

@Injectable()
export class AuthService {


private isUserloggedIn ;
  private userid ;


  constructor(private cookieservice:CookieService) {
    this.isUserloggedIn = false;

   }

setUserLoggedIn (user_id){
  this.cookieservice.put("userid", user_id);
  this.isUserloggedIn = true ;
}  

setUserLoggedOut(){
  this.isUserloggedIn = false;
  this.cookieservice.remove("userid");
}

getUserLoggedIn () {
  console.log(this.cookieservice.get("userid"));
  return (this.cookieservice.get("userid")!="" && this.cookieservice.get("userid")!=undefined);

}


}
