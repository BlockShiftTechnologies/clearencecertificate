import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule,Routes } from '@angular/router';
import { LoginComponent } from "../login/login.component" ;
import { SignupComponent } from "../signup/signup.component";
import { DashboardComponent } from "../dashboard/dashboard.component";
import { ListclearedstudentComponent } from "../listclearedstudent/listclearedstudent.component";
import { SearchcomponentComponent } from "../searchcomponent/searchcomponent.component";
import {dashboardRoutes} from '../dashboard/dashboard-routing.module';
import {AuthGuard} from '../auth.guard';
const appRoutes : Routes =[

{
	
	path: "",
  canActivate:[AuthGuard],
	redirectTo:"dashboard",
  pathMatch: 'full'


},


{
	
	path: "login",
    component: LoginComponent

},

{
	
	path: "signup",
	component: SignupComponent	


},

{
	path: "dashboard",
  canActivate : [AuthGuard], 
    component: DashboardComponent,
    children:[
    
    {
      path:"",
      canActivate: [AuthGuard],
      redirectTo:"search",
      pathMatch: 'full'
    },


    {
    		path: "list",
        canActivate : [AuthGuard], 
    		component: ListclearedstudentComponent
   },
   {
   			path: "search",
        canActivate : [AuthGuard], 
   			component: 	SearchcomponentComponent

   }
   ]

}

];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 

}
