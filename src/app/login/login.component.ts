import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import { NgForm } from '@angular/forms';
import {DataService} from '../data.service';
import {AuthService} from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {LogindialogComponent} from '../logindialog/logindialog.component';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private dataservice:DataService,private authservice:AuthService,private router:Router,public dialog: MatDialog ) { }

  ngOnInit() {
  }


signin(f:NgForm){
	
console.log('Admin name and password',f.value);
this.dataservice.loginadmin(f.value.username,f.value.password).subscribe(

data=>{
	console.log('Data:', data);
    if (data.body == 'Please signup' )
    	{
    		let dialogRef = this.dialog.open(LogindialogComponent, {
      		height: '200px',
      		width: '900px'
    	});
    	}

    	

    if (data.body == 'Incorrect password')
    	{
    	let dialogRef = this.dialog.open(LogindialogComponent, {
      height: '200px',
      width: '900px'
    });
    }
    	

    if (data.user_id != undefined) {
    	console.log('Successfully login ',data.user_id);
    	this.authservice.setUserLoggedIn(data.user_id);
        this.router.navigate(['dashboard']);
    }		

},
error=>{}

);


}

}
