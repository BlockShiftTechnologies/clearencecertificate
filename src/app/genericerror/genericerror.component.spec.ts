import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenericerrorComponent } from './genericerror.component';

describe('GenericerrorComponent', () => {
  let component: GenericerrorComponent;
  let fixture: ComponentFixture<GenericerrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenericerrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenericerrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
