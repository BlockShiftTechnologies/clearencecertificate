import { Component, OnInit,ViewChild, ViewContainerRef } from '@angular/core';
import { DataService } from '../data.service';
import {NgForm} from '@angular/forms';
import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import { DatatableComponent } from '@swimlane/ngx-datatable';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {GenericerrorComponent} from '../genericerror/genericerror.component';

@Component({
  selector: 'app-searchcomponent',
  templateUrl: './searchcomponent.component.html',
  styleUrls: ['./searchcomponent.component.css']
})

export class SearchcomponentComponent implements OnInit{

  constructor(private dataservice:DataService,public dialog: MatDialog) { }

  ngOnInit() {
  }

@ViewChild('studentTable') private table: DatatableComponent;
@ViewChild('defaulterTable') private defaultertable: DatatableComponent;


student = [];
student_rows = [];

defaulter =[];
defaulter_rows=[];

datatablehide = false;

searchfunctionality(f:NgForm){


	console.log("Form Value",f.value);
  this.datatablehide=true;
     this.dataservice.searchstudent(f.value.enrollment_no).subscribe(
    	(result:any)=>{
          
    		if(result.data == 'not found')
    		{
    		
    		let dialogRef = this.dialog.open(GenericerrorComponent, {
      		height: '200px',
      		width: '900px'
    	});



    		}
    		else {

          console.log("Result",result);
          this.student.push(result.data);
          this.student_rows = this.student;
          console.log("Student rows",this.student_rows);
          this.student_rows = [...this.student_rows];
          
          console.log("Defaulter data",result.defaulter);
          this.defaulter.push(result.defaulter);
          this.defaulter_rows = this.defaulter ;
          this.defaulter_rows = [...this.defaulter_rows];

        
      }
  }, 
);

     this.student=[];
     this.student_rows=[];
     
     this.defaulter =[];
	 this.defaulter_rows=[];
};






updatestatus(student_enrollmentno,status){
	
	this.dataservice.updatestatus(student_enrollmentno,status).subscribe(

	data=>{

    console.log("Data",data);

	}


	);

};

}



