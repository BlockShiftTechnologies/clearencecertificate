import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {FormGroup,FormsModule,FormControl,ReactiveFormsModule,FormBuilder,NgForm} from '@angular/forms';
import {DataService} from '../data.service';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,private dataservice:DataService) { }

  ngOnInit() {


  }


form:FormGroup;
username:FormControl;





signup(f: NgForm){

console.log("Form submitted",f.value.username);	
this.dataservice.registeradmin(f.value.username,f.value.password,f.value.email,f.value.designation).subscribe(

      data => {
      console.log('Data:', data.status)
       if (data.status == "Successfully registered") {
       console.log('User registered');
       }

      },
      err => console.log(err)

);

}


}
