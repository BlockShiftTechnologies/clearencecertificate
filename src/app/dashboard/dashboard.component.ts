import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {DomSanitizer} from '@angular/platform-browser';
import {MatIconRegistry} from '@angular/material';
import {AuthService} from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer,private authservice:AuthService,private router:Router) {

  		iconRegistry.addSvgIcon(
        'hamburger',
        sanitizer.bypassSecurityTrustResourceUrl('assets/hamburger.svg'));


      iconRegistry.addSvgIcon(
        'search',
        sanitizer.bypassSecurityTrustResourceUrl('assets/search.svg'));  
  

iconRegistry.addSvgIcon(
        'list',
        sanitizer.bypassSecurityTrustResourceUrl('assets/list.svg'));



  }


   

  ngOnInit() {
  }


mode = new FormControl('push');


logout(){
  
this.authservice.setUserLoggedOut();
this.router.navigate(['login']);

}

}
