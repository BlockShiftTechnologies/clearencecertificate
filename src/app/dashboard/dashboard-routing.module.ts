import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule,Routes } from '@angular/router';
import { ListclearedstudentComponent } from "../listclearedstudent/listclearedstudent.component";
export const dashboardRoutes : Routes =[


{
	
	path: "list",
	component: ListclearedstudentComponent

}

];

@NgModule({
  imports: [RouterModule.forChild(dashboardRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 

}
