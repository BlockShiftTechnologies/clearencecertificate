import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import {MatInputModule} from '@angular/material/input';

import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatDividerModule} from '@angular/material/divider';
import { DashboardComponent } from './dashboard/dashboard.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatRadioModule} from '@angular/material/radio';
import {MatIconModule} from '@angular/material/icon';
import {DashboardModule } from './dashboard/dashboard.module';
import { ListclearedstudentComponent } from './listclearedstudent/listclearedstudent.component';
import { SearchcomponentComponent } from './searchcomponent/searchcomponent.component';
import {MatGridListModule} from '@angular/material/grid-list';
import { DataService } from './data.service';
import { CookieModule } from 'ngx-cookie';
import {AuthService} from './auth.service';
import { AuthGuard } from './auth.guard';
import {MatDialogModule} from '@angular/material/dialog';
import { LogindialogComponent } from './logindialog/logindialog.component';
import {MatTableModule} from '@angular/material/table';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { GenericerrorComponent } from './genericerror/genericerror.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    DashboardComponent,
    ListclearedstudentComponent,
    SearchcomponentComponent,
    LogindialogComponent,
    GenericerrorComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    MatInputModule,
    FormsModule, 
    ReactiveFormsModule,
    MatFormFieldModule,
    DashboardModule ,
    MatSelectModule,
    MatToolbarModule,
    MatCardModule,
    MatButtonModule,
    MatDividerModule,
    MatSidenavModule,
    MatRadioModule,
    MatIconModule,
    MatGridListModule,
    MatDialogModule,
    MatTableModule,
    NgxDatatableModule,
    CookieModule.forRoot()


  ],
  providers: [DataService,AuthService,AuthGuard],
  bootstrap: [AppComponent],
  entryComponents:[LogindialogComponent,GenericerrorComponent]
})
export class AppModule { }
