import { Injectable } from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {AuthService } from './auth.service';
@Injectable()
export class AuthGuard implements CanActivate {


constructor(private authservice:AuthService,private router:Router){}


  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
  if (this.authservice.getUserLoggedIn()){
      return true;
  }  
  else{
    console.log("u r not authenticated");
      this.router.navigate(["/login"]);
  }

  }
}
